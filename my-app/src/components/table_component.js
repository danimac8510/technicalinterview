import React, { Fragment } from "react";
import "./table_component.css";

class Table_Data extends React.Component {
  state = {
    data_table: this.props.data,
    data_table_last: this.props.data,
  };

  click = (userID) => {
    console.log("----", userID);
  };

  componentDidMount() {
   // console.log("--a",this.state.data_table)

  }

  update_search = (event) => {
    //console.log("update")
    var filter_data = [];
    const data_search = this.state.data_table.map((a) => {
      const filter = Object.keys(a).map((b) => {
        if (a[b].includes(event.target.value.trim().toLowerCase())) {
          // console.log("!--->",a);
          filter_data.push(a);
        }
      });
    });

    this.setState({
      data_table: filter_data,
    });
    // console.log("--REVIEW",filter_data)
  };

  onKeyDown = (event) => {
    if (event.key === "Backspace") {
      console.log("delete");
      this.setState({
        data_table: this.state.data_table_last,
      });
    }
  };

  render() {
    const number_rows = this.props.rows * 50 + "px";
    return (
      <div className="TABLE_ALL_CONTAINER">
        <div className="ROW_CARD_table_USER_SEARCH" style={{ zIndex: 15 }}>
          <div className="ROW_USER_SEARCH">
            <div className="SEARCH_field">
              <input
                type="text"
                placeholder="Search"
                onChange={this.update_search}
                onKeyDown={this.onKeyDown}
              />
            </div>
          </div>
        </div>
        <div className="ROW_TABLE__TITTLE" style={{ zIndex: 20 }}>
          {this.props.head_table.map((a, indx) => {
            // console.log("--data:", Object.values(a));
            const margin_ = indx == 0 ? "10px" : "5px";
            return (
              <div
                className="COL_ROW_USER_TITLE"
                style={{ width: Object.values(a) }}
              >
                <div className="">
                  <span
                    className="ROW_TEXT_TITLE"
                    style={{ marginLeft: margin_ }}
                  >
                    <b>{Object.keys(a)}</b>
                  </span>
                </div>
              </div>
            );
          })}
        </div>

        <div
          className="row_table__CONTAINER"
          style={{ zIndex: 15, height: number_rows }}
        >
          {this.state.data_table.map((a, i, array) => {
            //console.log("index__:",Object.values(a));
            return (
              <div
                className="ROW_CARD__table_USER"
                style={{ zIndex: array.length - i + 10 }}
              >
                {Object.values(a).map((b, i_1) => {
                  //  console.log("--arr;",Object.values(this.props.head_table[i_1]))
                  if (this.props.hideFirst) {
                    if (i_1 > 0) {
                      return (
                        <div
                          className="COL1_ROW_USER"
                          style={{
                            width: Object.values(
                              this.props.head_table[i_1 - 1]
                            ),
                          }}
                        >
                          <div className="DIVIDER_row_card_ROW_USER">
                            <span className="ROW_TEXT">{Object.values(b)}</span>
                          </div>
                        </div>
                      );
                    }
                  } else {
                    return (
                      <div
                        className="COL1_ROW_USER"
                        style={{
                          width: Object.values(this.props.head_table[i_1]),
                        }}
                      >
                        <div className="DIVIDER_row_card_ROW_USER">
                          <span className="ROW_TEXT">{Object.values(b)}</span>
                        </div>
                      </div>
                    );
                  }
                })}

                <div
                  className="Col_card_1_ROW_USER_BTN"
                  style={{ width: "10%" }}
                >
                  <div
                    className="Col_card_1_COLUMN_USER_BTN"
                    style={{ width: "100%" }}
                  >
                    <div
                      className="Col_card_1_ROW_USER_BTN"
                      style={{ width: "50%" }}
                      onClick={()=>{
                        this.props.icon1Action(a)
                      }}
                    >
                      {this.props.icon_1}
                      {/* <MDBIcon icon="pencil-alt" size="lg"/>*/}
                    </div>
                    <div
                      className="Col_card_1_ROW_USER_BTN"
                      style={{ width: "50%" }}
                      onClick={() => {
                        this.props.icon2Action(a);
                      }}
                    >
                      {this.props.icon_2}
                      {/* <MDBIcon far icon="arrow-alt-circle-right" style={{"fontSize": "1.5em"}}/>*/}
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Table_Data;
