import React, { Fragment } from "react";
import './App.css';
import Table from "./components/table_component";
import head_table_data from "./components/data_head_table.json";
import data_table from './components/user_data_demo_table.json'; //'./components/user_data_demo_table_api.json'


/*

This technical inteview hat

*/

class App extends React.Component {

  state={
    data_table:data_table
  }

componentDidMount(){
  //call data api here, and update state.data_table
}

render(){
  return (
    <div className="App"/* style={{
      "display": "flex",
      "minHeight": "100vh",
      "alignItems": "center",
      "justifyContent": "center",
    }}*/>
      <div className="container" style={{
            "width": "1024px",
            "maxWidth": "1024px"
      }}>
        <Table
              data={this.state.data_table} //data from api
              head_table={head_table_data}
              hideFirst
              rows={4}
            />
      </div>

    </div>
  );
}

}


export default App;
